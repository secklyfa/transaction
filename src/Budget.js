import React from 'react'
import Header from './Components/header/Header';
import Input from './Components/Input';
import Liste from './Components/Transaction/Liste';

export default function Budget() {
  return (
    <div>
  <Header/>
  <Input/>
  <Liste/>

    </div>
  )
}
