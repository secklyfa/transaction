import React from 'react'
import { Link } from 'react-router-dom'

export default function Navbar() {
  return (
    <div>
         <nav class="main-header navbar navbar-expand navbar-dark navbar-dark">

    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
     <h4 style={{color:'white',marginTop:5}}>BUDGET</h4>
    </ul>

    
    <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
      
        <div class="input-group-append">
         
        </div>
      </div>
    </form>

    
    <ul class="navbar-nav ml-auto">
     
      <li class="nav-item dropdown">
       
       
         
         
           
      </li>
     
    
        {/* <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <span class="dropdown-item dropdown-header">15 Notifications</span>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
           
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            
            <span class="float-right text-muted text-sm">12 hours</span>
          </a>
         
          
        </div> */}
      {/* </li> */}
     
    </ul>
    <Link to='/Login'>  <button type="button" class="btn btn-danger">Déconnexion</button>
    </Link>
  
  </nav>
    </div>
  )
}
