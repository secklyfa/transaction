import React from 'react'

export default function Footer() {
  return (
    <div>
        <footer class="main-footer">
    <strong>Copyright &copy; 2021-2022 <a href="http://adminlte.io">Lyfa0000@gmail.com</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 3.0.1
    </div>
  </footer> 
    </div>
  )
}
