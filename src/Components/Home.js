import React from 'react'
import Header from './header/Header'
import Input from './Input'
import Liste from './Transaction/Liste'

export default function Home() {
  return (
    <div>
  <Header/>
  <Input/>
  <Liste/>
    </div>
  )
}
