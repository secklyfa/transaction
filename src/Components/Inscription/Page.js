import React from 'react'
import Navbar from '../Navbar';
import Sidebar from '../Sidebar';
import Home from '../Home';
import Footer from '../Footer'


export default function Page() {
  return (
    <div>
       <Navbar/>
       <Sidebar/>
       <Home/>
       <br/><br/><br/><br/><br/>
       <Footer/> 
        
    </div>
 
  )
}
