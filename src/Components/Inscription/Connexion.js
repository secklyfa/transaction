import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import 'bootstrap/dist/css/bootstrap.min.css'
import { Link, useNavigate } from "react-router-dom";
import firebase from "../../Firebase";
import swal from "sweetalert";
// import swal from ' sweetalert '   ;


const Connexion = () => {
	const validationSchema = Yup.object().shape({
		name: Yup.string()
			.required("ce champ est obligatoire")
			.min(2, "trop petit!")
			.max(50, "trop long!"),
			fristname: Yup.string()
			.required("ce champ est obligatoire")
			.min(2, "trop petit!")
			.max(50, "trop long!"),
		email: Yup.string()
			.email("email invalide")
			.required("l'email est obligatoire"),
      telephone: Yup.string()
			.required("Telephone est obligatoire")
			.matches(/([0-9])/, "Au moins un entier")
			.min(7, " Telephone doit être plus grand que 7 caractères")
			.max(
				14,
				"Telephone doit être plus petit que 50 caractères"
			),
		password: Yup.string()
			.required("Mot de passe est obligatoire")
			.matches(/([0-9])/, "Au moins un entier")
			.min(8, "Mot de passe doit être plus grand que 8 caractères")
			.max(
				50,
				"Mot de passe doit être plus petit que 50 caractères"
			),
		confirmPassword: Yup.string().oneOf(
			[Yup.ref("password"), null],
			"Le mot de passe de confirmation ne correspond pas"
		),
		
	});
  const [name,setName] = useState('');
   const [fristname,setFristname] = useState('');
   const [email,setEmail] = useState('');
  const [telephone,setTelephone] = useState('');
  const [password,setPassword] = useState('');
  const [confirmPassword,setConfirmPassword] = useState('');
	const navigate =useNavigate()

  const add=()=>{
    firebase.database().ref('Users').set({name,fristname,email,telephone,password,confirmPassword})
	.then(()=>{
		navigate('/login')
	})
	.catch(alert)
     
    };
 

	const { register, handleSubmit, formState, reset } = useForm({
		mode: "onBlur",
		defaultValues: {
			name: "",
      fristname:"",
			email: "",
      telephone:"",
			password: "",
			confirmPassword: "",
			acceptTerms: false,
		},
		resolver: yupResolver(validationSchema),
		
	});

	const { errors } = formState;

	const onSubmit = (data) => {
    console.log(data);
    reset()
	};

	const sweetalert=()=>{
		swal({
			title:"utilisateur est enrigistre avec success",
			icon:'success',
			button:'Accepter'
		})
	}
	

	return (




		
		<div className="container pt-4 "  >
			<div class="row">
  <div class="col-sm-6">
 <h1>Bienvenue!!</h1>
  </div>
  <div class="col-sm-6">
    <div class="card">
      <div class="card-body">
	  <div className="row">
				<div className="col-md-6 offset-md-3">
					<form onSubmit={handleSubmit(add)}>
						<h1 className="text-center">Inscription</h1>
						<div className="form-group mb-3">
							{/* <label htmlFor="name">
								Nom :
							</label> */}
							<input
								type="text"
								className="form-control" placeholder="Entre votre nom"
								{...register("name")}
								name="name"
								id="name"
                value={name}
                onChange={(e)=>setName(e.target.value)}
							/>
							<small className="text-danger">
								{errors.name?.message}
							</small>
						</div>
            <div className="form-group mb-3">
							
							<input
								type="text"
								className="form-control" placeholder="Entre votre prenom"
								{...register("fristname")}
								name="fristname"
								id="fristname" value={fristname} onChange={(e)=>setFristname(e.target.value)}
							/>
							<small className="text-danger">
								{errors.fristname?.message}
							</small>
						</div>
						<div className="form-group mb-3">
						
							<input
								type="email"
								className="form-control" placeholder="Entre votre adresse email"
								{...register("email")} id='email' value={email} onChange={(e)=>setEmail(e.target.value)}
							/>
							<small className="text-danger">
								{errors.email?.message}
							</small>
						</div>
            <div className="form-group mb-3">
							
							<input
								type="tel"
								className="form-control" placeholder="Entre votre numero telephone"
								{...register("telephone")} value={telephone} onChange={(e)=>setTelephone(e.target.value)}
							/>
							<small className="text-danger">
								{errors.telephone?.message}
							</small>
						</div>
						<div className="form-group mb-3">
						
							<input
								type="password"
								className="form-control" placeholder="Entre votre mot de passe"
								{...register("password")}
								name="password"
								id="password" value={password} onChange={(e)=>setPassword(e.target.value)}
							/>
							<small className="text-danger">
								{errors.password?.message}
							</small>
						</div>
						<div className="form-group mb-3">
							
							<input
								type="password"
								className="form-control" placeholder="Confirmer votre mot de passe"
								{...register("confirmPassword")}
								name="confirmPassword"
								id="confirmPassword" value={confirmPassword} onChange={(e)=>setConfirmPassword(e.target.value)}
							/>
							<small className="text-danger">
								{
									errors.confirmPassword
										?.message
								}
							</small>
						</div>
						<div className="form-check">
							<small className="text-danger d-block">
								{errors.acceptTerms?.message}
							</small>
						</div>
						<div className="form-group d-flex justify-content-center mt-4 justify-content-md-end gap-3">
						
           
            <button 
								type="submit"
								className="btn btn-primary" onClick={()=>sweetalert()}
								
							>
								S'inscrire
							</button>
            
          
							<button
								type="button"
								className="btn btn-danger"
								onClick={() => reset()}
							>
								Annuler
							</button>
						</div>
					</form>
				</div>
			</div>
      </div>
    </div>
  </div>
</div>
			<div className="messages">
  {/* {errorMessage()}
  {successMessage()}
  */}
</div> 
		
		</div>
	);
};

export default Connexion;