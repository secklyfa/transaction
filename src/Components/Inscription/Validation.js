export default function Validation(values){
    let errors={}

    if(!values.nom.trim()) {
        errors.nom='nom required'
    }

    if(!values.email){
        errors.email='Email.required'
    }else if (!/^[A-Z0-9,_%+-]+@[A-Z0-9,-]+\.[A-Z]{2,}$/i.test(values.email))
    {
        errors.email = 'Adresse email invalide'
    }
       
   
    if (!values.password){
        errors.password= 'password required'
    }else if (values.password.lenght =6){
        errors.password= 'le mot de passe doit avoir 6 caracteres '

    }
    if (!values.confirmation){
        errors.confirmation='password is required'
    }else if (values.confirmation!== values.password){
        errors.confirmation='mot de passe non conforme'
    }

    return errors;
}


