import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import 'bootstrap/dist/css/bootstrap.min.css'
import { Link } from "react-router-dom";
import firebase from "../../Firebase";
import swal from "sweetalert";


const Login = () => {
	const validationSchema = Yup.object().shape({
	
      telephone: Yup.string()
			.required("Telephone est obligatoire")
			.matches(/([0-9])/, "Au moins un entier")
			.min(7, " Telephone doit être plus grand que 7 caractères")
			.max(
				14,
				"Telephone doit être plus petit que 50 caractères"
			),
		password: Yup.string()
			.required("Mot de passe est obligatoire")
			.matches(/([0-9])/, "Au moins un entier")
			.min(8, "Mot de passe doit être plus grand que 8 caractères")
			.max(
				50,
				"Mot de passe doit être plus petit que 50 caractères"
			),
		
	
	});
 
  const [telephone,setTelephone] = useState('');
  const [password,setPassword] = useState('');
  
  
  const cpt=0;
  const ajouter=()=>{
    firebase.database().ref('User'+cpt).set({telephone,password}).catch(alert)
     cpt++;
    };
 

	const { register, handleSubmit, formState, reset } = useForm({
		mode: "onBlur",
		defaultValues: {
		
      telephone:"",
			password: "",
		
			acceptTerms: false,
		},
		resolver: yupResolver(validationSchema),
	});

	const { errors } = formState;

	const onSubmit = (data) => {
    console.log(data);
    reset()
	};

	const successMessage = () => {
		return (
		  <div
			className="success"
			
			>
				<h1 style={{color:'green'}}>L'utilisateur  s'est enregistré avec succès !!!</h1>
		
		  </div>
		);
	  };

	return (
		
		<div className="container pt-4"  style={{background:'white',width:500,borderRadius:10}}>
			<div className="messages">

  {/* {successMessage()} */}
  
</div> 
			
			<div className="row">
				<div className="col-md-6 offset-md-3">
					<form onSubmit={handleSubmit(onSubmit)}>
						<h1 className="text-center">Connexion</h1>
					
          
				
            <div className="form-group mb-3">
							<label htmlFor="tel">Telephone:</label>
							<input
								type="tel"
								className="form-control"
								{...register("telephone")} value={telephone} onChange={(e)=>setTelephone(e.target.value)}
							/>
							<small className="text-danger">
								{errors.telephone?.message}
							</small>
						</div>
						<div className="form-group mb-3">
							<label htmlFor="password">
								password:
							</label>
							<input
								type="password"
								className="form-control"
								{...register("password")} placeholder='Entre votre mot de passe'
								name="password"
								id="password" value={password} onChange={(e)=>setPassword(e.target.value)}
							/>
							<small className="text-danger">
								{errors.password?.message}
							</small>
						</div>
					
						<div className="form-check">
							
						
							<small className="text-danger d-block">
								{errors.acceptTerms?.message}
							</small>
						</div>
						<div className="form-group d-flex justify-content-center mt-4 justify-content-md-end gap-3">
						
            <Link to='/Page'>
            <button
								type="submit"
								className="btn btn-primary" onClick={ajouter}
							>
								Se Connecte
							</button>
            </Link>
            
          
							<button
								type="button"
								className="btn btn-danger"
								onClick={() => reset()}
							>
								Annuler
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	);
};

export default Login;