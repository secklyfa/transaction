import React from 'react'
import useFirestore from '../../firebase/useFirestore'
import './Header.css'

export default function Header() {
  const {budget}=useFirestore();
  return (
    <div className='header'>
    
  <div class="col-sm-4">
    <div class="card">
      <div class="card-body">
      <h4 >budget actuelle</h4>
      <div className='balance' style={{background:'royalblue',color:'white'}}>{budget}CFA</div><br/>
      </div>
    </div>
  </div>

    </div>
  )
}
