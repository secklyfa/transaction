import React from 'react'
import './Item.css';
import useFirestore from '../../firebase/useFirestore';

const Item=(item)=> {
    const [deleteItem]=useFirestore();
  return (
    <div className='item'>
        <div className='item-description'>
         <h3>{item.description}</h3>
        </div>

        <div className='item_info'>
            <p>{item.date}</p>
            <p className={item.montant >=0 ?'revenu' : 'depense'}>{item.montant}CFA
            {Math.abs(item.montant)}  </p>
            <p>{item.type}</p>
            <button onClick={deleteItem}>delete</button>
        </div>

    </div>
  )
}
 export default Item;