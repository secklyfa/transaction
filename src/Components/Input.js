
import { useState,useRef } from 'react'
import useFirestore from '../firebase/useFirestore';
import './Input.css'


const initialItem = {description:'', date:'', montant:'', type:''};

const Input=() =>{
    const {addItem} = useFirestore();
    const [item,setItem]=useState(initialItem);
    const [montant,setMontant]=useState('');
    const selectOption = useRef(null);

    const handleChange=({target})=>{
        setItem({
            ...item,
            [target.name]:target.value,
            type: selectOption.current.value,
        });

    };
    const handleMontant=({target})=>{
        setMontant(target.value);
    };

   const handleSubmit=async (e)=>{
       e.preventDefault()
       let actuelMontant=
       selectOption.current.value==='Revenu'? Math.abs(parseInt(montant)): -parseInt(montant);

       await addItem(item, actuelMontant);
       setItem(initialItem);
       setMontant('');
   }


  return (
    <div className='input ml-5'style={{textAlign:'center'}} >
        <form onSubmit={handleSubmit}>
            <input className='text' type='text' name='description' placeholder='Entre la description' onChange={handleChange} required autoComplete='off'
        />
            <input  className='date' type='date' name='date' placeholder='Entre la date' onChange={handleChange} required autoComplete='off'/>
            <input   className='montant' type='number' name='montant' placeholder=' Entre le montant'  onChange={handleMontant} required autoComplete='off'/>
        
        
        <label htmlFor='type' style={{color:'royalblue'}}>type</label>
        <select name='type' onChange={handleChange} ref={selectOption}>
        <option value='Revenu'>Revenu</option>
        <option value='Dépense'>Dépense</option>
        
        </select >
        <input  className='btn-primary ' type='submit' value='Ajouter' style={{width:100,height:30,borderRadius:20}}></input>
        
        </form>

    </div>
  )
}

export default  Input;
