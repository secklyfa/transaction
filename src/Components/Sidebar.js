import React from 'react'
import { Link } from 'react-router-dom'

export default function Sidebar() {
  return (
    <div>
         <aside class="main-sidebar sidebar-dark-primary elevation-4">
   
   

   
    <div class="sidebar">
    
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
        
        </div>
        <div class="info">
      
        </div>
      </div>

     
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
         
          <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                BUDGET
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              
            
            
            </ul>
          </li>

          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-envelope"></i>
              <p>
                REINITIALISER
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
          
          </li>
        
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
             
            
            </a>
          
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>
                VALEUR INITIAL
                <i class="right fas fa-angle-left"></i><br/>
                <button type="button" class="btn btn-primary">500000CFA</button>
              </p>
            </a>
           
          </li><br/>
          <li class="nav-item has-treeview">
          </li>
          <li class="nav-item has-treeview">
            <a href="/Login" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <button>
              TRANSACTIONS
                <i class="fas fa-angle-left right"></i>
          
              </button>
            </a>
           
          </li><br/>
          <li class="nav-item has-treeview">
            <a href="" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>
              BUDGET ACTUEL
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
           
          </li><br/>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-plus-square"></i>
              <p>
                <Link to='./Input'>
               <button> AJOUT TRANSACTION</button>
               </Link>
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
            </ul>
          </li><br/>
          <li class="nav-item has-treeview">
            <ul class="nav nav-treeview">
              
            </ul>
          </li>
        </ul>
      </nav>
      
    </div>
  
  </aside>
    </div>
  )
}
