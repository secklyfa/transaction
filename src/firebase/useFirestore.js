import React, {useEffect,useState} from 'react'
import {db} from '../Firebase'

 const useFirestore=()=> {
     const [items,setItems]=useState([]);
     const [budget,setBudget]=useState(50000)


 useEffect (()=>{
  const unsubscribe=db.collection('items').orderBy('date').onSnapshot(snap=>{
         let fetched= snap.docs.map((doc)=>{
             return{...doc.data(), id: doc.id}
         });

         let budget = snap.docs.map(doc=>{
             return doc.data().montant
         })
         setItems(fetched)  
         setBudget ( budget.reduce((acc, curr)=>acc+curr),budget)
     })
     return unsubscribe
 },[]);

 const addItem = async(items, montant)=>{
     await db.collection('items').add({
         ...items,
         montant,
     });
 };
 const deleteItem=async (id)=>{
     await db.collection('Items').doc(id).delete();
 }
 return {items, addItem,deleteItem,budget}
}
export default useFirestore;