import firebase from 'firebase';
// import 'firebase/firestore';


const firebaseConfig = {
    apiKey: "AIzaSyDukMuIqjxGzSvsUylTnvB5QoNZuJsa2uk",
    authDomain: "budget-91b65.firebaseapp.com",
    projectId: "budget-91b65",
    storageBucket: "budget-91b65.appspot.com",
    messagingSenderId: "1023758725186",
    appId: "1:1023758725186:web:7be9f136893e784b043ddb",
    measurementId: "G-BLW290M1DS"
  };

  firebase.initializeApp(firebaseConfig);

  export const db = firebase.firestore();
  export default firebase;