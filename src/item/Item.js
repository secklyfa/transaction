import React from 'react'
import { useRef } from 'react';
import useFirestore from '../firebase/useFirestore';

const Item=({item}) =>{
    const {deleteItem}= useFirestore();
    const delBtn=useRef()
  return (
    <div 
    className='item'
    onMouseEnter={() =>(deleteItem.current.style.display = 'block')}
    onMouseLeave={() =>(deleteItem.current.style.display = 'none')}>
        <div className='item_title'>
            <h3>{item.description}</h3>

        </div>
        <div className='item_info'>
           
             <p className={item.montant <=0 ? 'revenu': 'depense'}>
               {Math.abs(item.montant)} CFA</p>
            <p>{item.date}</p> 
            <button onClick={()=>deleteItem(item.id)} className='item_delete' ref={delBtn}>delete</button>

        </div>

    </div>
  )
}
export default Item;